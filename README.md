# React Task to build a Pricing page 

This task doesnot conatain usage of state, its a [seperate repository](https://gitlab.com/TusharYaar/react-pricing-state-task "React Pricing State Task") 


## Question
Create pricing page using react

## Solution
Screenshots: 

![Screenshot 1](screenshots/Screenshot%20(227).png)
![Screenshot 2](screenshots/Screenshot%20(228).png)
![Screenshot 3](screenshots/Screenshot%20(229).png)



## Avalible Scripts

#### `npm start`
Runs the app in the development mode.
Open (http://localhost:3000)[http://localhost:3000] to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

