import "./App.css";

import FooterSection from "./components/FooterSection";
import Header from "./components/Header";
import PriceCard from "./components/PriceCard";

import { pricingPlans, footer } from "./data";

function App() {
  return (
    <div className="App">
      <Header title="Pricing">
        Quickly build an effective pricing table for your potential customers with this layout. It's built with default
        Material-UI components with little customization.
      </Header>
      <div className="pricing-container">
        {pricingPlans.map((plan, index) => (
          <PriceCard key={index} {...plan} />
        ))}
      </div>
      <hr className="horizontal-rule" />
      <div className="footer-container">
        {footer.map((section, index) => (
          <FooterSection key={index} {...section} />
        ))}
      </div>
    </div>
  );
}

export default App;
