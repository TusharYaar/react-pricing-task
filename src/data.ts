import type { Props as PriceCardProps } from "./components/PriceCard";

const pricingPlans: PriceCardProps[] = [
    {
      title: "Free",
      price: 0,
      features: ["10 users included", "2 GB of storage", "Help center access", "Email support"],
      buttonText: "Sign up for free",
      buttonVariant: "outlined",
      isStarted: false,
    },
    {
      title: "Pro",
      price: 15,
      features: ["20 users included", "10 GB of storage", "Help center access", "Priority email support"],
      buttonText: "Get started",
      buttonVariant: "contained",
      isStarted: true,
    },
    {
      title: "Enterprise",
      price: 30,
      features: ["50 users included", "30 GB of storage", "Help center access", "Phone & email support"],
      buttonText: "Contact us",
      buttonVariant: "outlined",
      isStarted: false,
    },
  ];
  
  const footer = [
    {
      title: "Company",
      links: [
        {
          text: "Team",
          href: "#",
        },
        {
          text: "History",
          href: "#",
        },
        {
          text: "Contact us",
          href: "#",
        },
        {
          text: "Locations",
          href: "#",
        },
      ],
    },
    {
      title: "Features",
      links: [
        {
          text: "Cool stuff",
          href: "#",
        },
        {
          text: "Random feature",
          href: "#",
        },
        {
          text: "Team feature",
          href: "#",
        },
        {
          text: "Developer stuff",
          href: "#",
        },
        {
          text: "Another one",
          href: "#",
        },
      ],
    },
    {
      title: "Resources",
      links: [
        {
          text: "Resource",
          href: "#",
        },
        {
          text: "Resource name",
          href: "#",
        },
        {
          text: "Another resource",
          href: "#",
        },
        {
          text: "Final resource",
          href: "#",
        },
      ],
    },
    {
      title: "Legal",
      links: [
        {
          text: "Privacy policy",
          href: "#",
        },
        {
          text: "Terms of use",
          href: "#",
        },
      ],
    },
  ];
  
  export { pricingPlans, footer };