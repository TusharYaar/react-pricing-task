import React from "react";
import "./styles.css";

interface Props {
  title: string;
  links: {
    text: string;
    href: string;
  }[];
}

const index = (props: Props) => {
  return (
    <div className="footer-section">
      <h4 className="footer-section-title">{props.title}</h4>
      <ul className="footer-section-list">
        {props.links.map((link, index) => {
          return (
            <li key={index}>
              <a href={link.href}>{link.text}</a>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default index;
