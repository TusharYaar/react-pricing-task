import "./styles.css";
const Button = ({ title, type }: { title: string; type: "outlined" | "contained" }) => {
  return <button className={`button ${type}`}>{title}</button>;
};

export default Button;
