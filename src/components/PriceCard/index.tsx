import "./styles.css";

import StarImg from "../../star.png";

import Button from "../Button";
export interface Props {
  title: string;
  price: number;
  features: string[];
  buttonText: string;
  buttonVariant: "outlined" | "contained";
  isStarted: boolean;
}

const PriceCard = (props: Props) => {
  return (
    <div className="price-card">
      <div className="price-card-header">
        {props.isStarted && <img className="price-card-header-star" src={StarImg} alt="star" />}
        <h3 className="price-card-title">{props.title}</h3>
        {props.isStarted && <p className="price-card-most-popular">Most Popular</p>}
      </div>
      <div className="price-card-body">
        <h1 className="price-card-price">
          ${props.price}
          <span className="price-card-mo">/mo</span>
        </h1>
        <ul className="price-card-features">
          {props.features.map((feature, index) => (
            <li key={index}>{feature}</li>
          ))}
        </ul>
        <Button title={props.buttonText} type={props.buttonVariant} />
      </div>
    </div>
  );
};

export default PriceCard;
